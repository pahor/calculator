﻿using System;
using System.Threading.Tasks;

namespace ExpressionEvaluator
{
    public interface IEvaluatorService
    {
        /// <summary>
        /// Evalueates string expression in infix order
        /// </summary>
        /// <param name="s">expression</param>
        /// <returns>result</returns>
        Task<int> Evaluate(String s);
    }
}
