﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEvaluator
{
    public class EvaluatorService : IEvaluatorService
    {      
        private bool IsOperator(char c)
        {
            return c == '+' || c == '-' || c == '/' || c == '*';
        }

        private bool IsOperand(char c)
        {
            return Char.IsDigit(c);
        }

        private bool IsOpeningBracket(char c)
        {
            return c == '(';
        }
        private bool IsClosingBracket(char c)
        {
            return c == ')';
        }
        
        public async Task<int> Evaluate(String s)
        {
            return await Task.Run(() =>
            {
                var operators = new Stack<char>();
                var operands = new Stack<int>();

                var sb = new StringBuilder();

                for (int i = 0; i < s.Length; i++)
                {
                    var c = s[i];
                    if (Char.IsWhiteSpace(c))
                    {
                        continue;
                    }
                    else if (IsOperand(c))
                    {
                        sb.Append(c);
                        while ((i + 1) < s.Length && IsOperand(c = s[i + 1]))
                        {
                            sb.Append(c);
                            i++;
                        }
                        operands.Push(int.Parse(sb.ToString()));
                        sb.Clear();
                    }
                    else if (IsOperator(c))
                    {
                        if (operators.Count > 0 && IsPreffered(operators.Peek(), c))
                        {
                            operands.Push(DoOpration(operators.Pop(), operands.Pop(), operands.Pop()));
                        }
                        operators.Push(c);
                    }
                    else if (IsOpeningBracket(c))
                    {
                        operators.Push(c);
                    }
                    else if (IsClosingBracket(c))
                    {
                        char oper;
                        while ((oper = operators.Pop()) != '(')
                        {
                            operands.Push(DoOpration(oper, operands.Pop(), operands.Pop()));
                        }
                    }
                    else
                    {
                        throw new NotSupportedException($"Characted {c} not supported");
                    }
                }
                return EvaluateInternal(operators, operands);
            });
        }

        private bool IsPreffered(char operator1, char operator2)
        {
            if ((operator2 == '*' || operator2 == '/') && (operator1 == '+' || operator1 == '-')
                || (operator1 == '(' || operator2 == ')'))
            {
                return false;
            }
            return true;
        }

        private int EvaluateInternal(Stack<char> operators, Stack<int> operands)
        {
            while (operators.Count > 0)
            {
                operands.Push(DoOpration(operators.Pop(), operands.Pop(), operands.Pop()));
            }
            return operands.Pop();
        }

        private int DoOpration(char op, int operand1, int operand2)
        {
            switch (op)
            {
                case '+':
                    return operand2 + operand1;
                case '-':
                    return operand2 - operand1;
                case '*':
                    return operand2 * operand1;
                case '/':
                    return operand2 / operand1;
                default:
                    throw new NotSupportedException($"Operation {op} not supported");
            }
        }
    }
}
