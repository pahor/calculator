﻿
using System;
using System.Threading.Tasks;
using ExpressionEvaluator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEvaluatorUnitTests
{
    [TestClass]
    public class ExpressionEvaluatorTests
    {        
        EvaluatorService tree = new EvaluatorService();

        [TestMethod]
        public async Task SimpleAdd()
        {
            (await tree.Evaluate("3 + 5")).Should().Be(8);
            (await tree.Evaluate("5 + 3")).Should().Be(8);
            (await tree.Evaluate("15 + 5")).Should().Be(20);
            (await tree.Evaluate("3 + 15")).Should().Be(18);
            (await tree.Evaluate("111 + 11")).Should().Be(122);

            (await tree.Evaluate("111 + 222 + 333")).Should().Be(666);
        }

        [TestMethod]
        public async Task SimpleAddition()
        {
            (await tree.Evaluate("3 * 5")).Should().Be(15);
            (await tree.Evaluate("5 * 3")).Should().Be(15);
            (await tree.Evaluate("15 * 5")).Should().Be(75);
            (await tree.Evaluate("3 * 15")).Should().Be(45);
            (await tree.Evaluate("111 * 11")).Should().Be(1221);
            (await tree.Evaluate("111 * 222 * 333")).Should().Be(8205786);
        }

        [TestMethod]
        public async Task SimpleSubtraction()
        {
            (await tree.Evaluate("3 - 2")).Should().Be(1);
            (await tree.Evaluate("5 - 3")).Should().Be(2);
            (await tree.Evaluate("15 - 5")).Should().Be(10);
            (await tree.Evaluate("3 - 15")).Should().Be(-12);
            (await tree.Evaluate("111 - 11")).Should().Be(100);
            (await tree.Evaluate("333 - 222 - 111")).Should().Be(0);
        }

        [TestMethod]
        public async Task SimpleDivision()
        {
            (await tree.Evaluate("10 / 2")).Should().Be(5);
            (await tree.Evaluate("45 / 9")).Should().Be(5);
            (await tree.Evaluate("100 / 10 / 10")).Should().Be(1);
        }

        [TestMethod]
        public async Task Parenthesis()
        {
            (await tree.Evaluate("5 - (10 + 2)")).Should().Be(-7);
            (await tree.Evaluate("60 / (2 * 5)")).Should().Be(6);
            (await tree.Evaluate("(50 + 10) * 2")).Should().Be(120);
        }

        [TestMethod]
        public async Task TestComplex()
        {
            (await tree.Evaluate("33 * 3 + 10")).Should().Be(109);
            (await tree.Evaluate("10 + 3 * 4")).Should().Be(22);
            (await tree.Evaluate("7 * ( 1 + 6 )")).Should().Be(49);
            (await tree.Evaluate("5 * ( 2 + 13 ) / 5")).Should().Be(15);
        }
    }
}
