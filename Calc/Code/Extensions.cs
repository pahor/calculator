﻿using System.Collections.Generic;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml.Input;

namespace Calc
{
    public static class Extensions
    {
        private static readonly Dictionary<VirtualKey, int> NumericKeys =
                                new Dictionary<VirtualKey, int> {
                                    { VirtualKey.Number0, 0 },
                                    { VirtualKey.Number1, 1 },
                                    { VirtualKey.Number2, 2 },
                                    { VirtualKey.Number3, 3 },
                                    { VirtualKey.Number4, 4 },
                                    { VirtualKey.Number5, 5 },
                                    { VirtualKey.Number6, 6 },
                                    { VirtualKey.Number7, 7 },
                                    { VirtualKey.Number8, 8 },
                                    { VirtualKey.Number9, 9 },
                                    { VirtualKey.NumberPad0, 0 },
                                    { VirtualKey.NumberPad1, 1 },
                                    { VirtualKey.NumberPad2, 2 },
                                    { VirtualKey.NumberPad3, 3 },
                                    { VirtualKey.NumberPad4, 4 },
                                    { VirtualKey.NumberPad5, 5 },
                                    { VirtualKey.NumberPad6, 6 },
                                    { VirtualKey.NumberPad7, 7 },
                                    { VirtualKey.NumberPad8, 8 },
                                    { VirtualKey.NumberPad9, 9 },
                               };

        private static readonly Dictionary<VirtualKey, char> Operators =
             new Dictionary<VirtualKey, char> {
                                    { VirtualKey.Add, '+' },
                                    { VirtualKey.Subtract, '-' },
                                    { VirtualKey.Multiply, '*' },
                                    { VirtualKey.Divide, '/' },
             };

        public static int? GetKeyNumericValue(this KeyEventArgs e)
        {
            if (NumericKeys.ContainsKey(e.VirtualKey))
            {
                return NumericKeys[e.VirtualKey];
            }

            return null;
        }

        public static char? GetSupportedOperation(this KeyEventArgs e)
        {
            if (Operators.ContainsKey(e.VirtualKey))
            {
                return Operators[e.VirtualKey];
            }
            return null;
        }

        public static char? GetBrackets(this KeyEventArgs e)
        {           
            if (e.IsLeftBracket())
            {
                return '(';
            }
            else if (e.IsRightBracket())
            {
                return ')';
            }

            return null;
        }

        public static bool IsLeftBracket(this KeyEventArgs e)
        {
            var inputLanguage = Windows.Globalization.Language.CurrentInputMethodLanguageTag;
            var state = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift);
            var shiftPressed = (state & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;

            switch (inputLanguage)
            {
                case "cs":
                    return e.VirtualKey == (VirtualKey)221 && shiftPressed;
                case var s when s.StartsWith("en"):
                    return e.VirtualKey == VirtualKey.Number9 && shiftPressed;
            }
            return false;
        }

        public static bool IsRightBracket(this KeyEventArgs e)
        {
            var inputLanguage = Windows.Globalization.Language.CurrentInputMethodLanguageTag;
            var state = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift);
            var shiftPressed = (state & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;

            switch (inputLanguage)
            {
                case "cs":
                    return e.VirtualKey == (VirtualKey)221 && !shiftPressed;
                case var s when s.StartsWith("en"):
                    return e.VirtualKey == VirtualKey.Number0 && shiftPressed;
            }
            return false;
        }
    }
}
