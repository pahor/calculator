﻿using Autofac;
using ExpressionEvaluator;
using NLog;
using System;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Calc
{
    public sealed partial class MainPage : Page
    {
        private readonly IEvaluatorService _evaluatorService;
        private readonly Logger _logger;

        private string currentExpression = string.Empty;
        private LastAction lastAction = LastAction.None;
        private LastAction previousAction = LastAction.None;

        public MainPage()
        {
            this.InitializeComponent();
            _evaluatorService = App.Container.Resolve<IEvaluatorService>();
            _logger = App.Container.Resolve<Logger>();
            _logger.Info("Started");
            Window.Current.CoreWindow.KeyUp += Global_KeyUp;
        }

        private async void Global_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.VirtualKey == VirtualKey.Enter)
            {
                await BtnEnter();
                e.Handled = true;
                btnEqual.Focus(FocusState.Programmatic);
            }
            else if (e.VirtualKey == VirtualKey.Back)
            {
                BtnBack();
            }
            else
            {
                var bracket = e.GetBrackets();
                if (bracket.HasValue)
                {
                    HandleBracket(bracket);
                    return;
                }

                var number = e.GetKeyNumericValue();
                if (number.HasValue)
                {
                    HandleNumber(number.Value);
                    return;
                }

                var operation = e.GetSupportedOperation();
                if (operation.HasValue)
                {
                    HandleOperation(operation);
                    return;
                }
            }
        }

        private void Erase()
        {
            currentExpression = string.Empty;
            lastAction = previousAction = LastAction.None;
            tbText.Text = string.Empty;
            tbExpressionText.Text = string.Empty;
        }

        private void HandleNumber(int number)
        {
            if (lastAction == LastAction.None)
            {
                tbText.Text = string.Empty;
            }
            if (lastAction != LastAction.RightBracket)
            {
                tbText.Text += number;
                lastAction = LastAction.Number;
            }
        }

        private void BtnBack()
        {
            if (tbText.Text.Length > 0 && lastAction != LastAction.None)
            {
                tbText.Text = tbText.Text.Remove(tbText.Text.Length - 1);
                if (tbText.Text.Length == 0)
                {
                    lastAction = previousAction;
                }
            } 
        }

        private async Task BtnEnter()
        {
            if (lastAction == LastAction.Number || lastAction == LastAction.RightBracket)
            {
                HandleOperation(' ');
                if (!string.IsNullOrEmpty(currentExpression.Trim()))
                {
                    try
                    {
                        tbText.Text = (await _evaluatorService.Evaluate(currentExpression)).ToString();
                        currentExpression = string.Empty;
                        lastAction = LastAction.None;
                    }
                    catch (Exception e)
                    {
                        _logger.Error($"{currentExpression} - error: {e}");
                        var dialog = new MessageDialog("Invalid input");
                        await dialog.ShowAsync();
                        Erase();
                    }
                }
            }
        }

        private void HandleOperation(char? operation)
        {
            if (lastAction == LastAction.RightBracket || lastAction == LastAction.Number)
            {
                currentExpression += $"{tbText.Text} {operation.Value} ";
                tbExpressionText.Text = currentExpression;
                tbText.Text = string.Empty;
                lastAction = previousAction = LastAction.Operation;
            }
        }

        private void HandleBracket(char? operation)
        {
            var bracketType = operation == '(' ? LastAction.LeftBracket : LastAction.RightBracket;

            if ((lastAction == LastAction.Number && bracketType == LastAction.RightBracket) || lastAction != LastAction.Number)
            {
                if (lastAction == LastAction.None)
                {
                    tbText.Text = string.Empty;
                }
                currentExpression += $"{tbText.Text} {operation.Value} ";
                tbExpressionText.Text = currentExpression;
                tbText.Text = string.Empty;
                lastAction = previousAction = bracketType;
            }
        }

        private async void BtnEqual_Click(object sender, RoutedEventArgs e)
        {
            await BtnEnter();
        }

        private void BtnBackspace_Click(object sender, RoutedEventArgs e)
        {
            BtnBack();
        }

        private void BtnDivide_Click(object sender, RoutedEventArgs e)
        {
            HandleOperation('/');
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void BtnMultiply_Click(object sender, RoutedEventArgs e)
        {
            HandleOperation('*');
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void BtnMinus_Click(object sender, RoutedEventArgs e)
        {
            HandleOperation('-');
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(1);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(2);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(3);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void BtnPlus_Click(object sender, RoutedEventArgs e)
        {
            HandleOperation('+');
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(4);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(5);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(6);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(7);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(8);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(9);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            HandleNumber(0);
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void BtnLeftBracket_Click(object sender, RoutedEventArgs e)
        {
            HandleBracket('(');
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void BtnRightBracket_Click(object sender, RoutedEventArgs e)
        {
            HandleBracket(')');
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            btnEqual.Focus(FocusState.Programmatic);
        }

        private void BtnErase_Click(object sender, RoutedEventArgs e)
        {
            Erase();
            btnEqual.Focus(FocusState.Programmatic);
        }
    }
}
